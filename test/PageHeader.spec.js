import { shallowMount, RouterLinkStub } from '@vue/test-utils';
import PageHeader from '@/components/PageHeader';

describe('PageHeader', () => {
  test('mounts without crashing', () => {
    const wrapper = shallowMount(PageHeader, {
      stubs: {
        NuxtLink: RouterLinkStub
      }
    });
    expect(wrapper.vm).toBeTruthy();
  })
})
