// mock data to test the app without connection to the API
// import mockCarData from '@/mocks/carData.json';

const actions = {
  updateCarData: async ({ commit, state }, { vinNumbers, http }) => {
    commit('setIsLoading');
    // Max 5 cars in the list
    const newVinNumbers = vinNumbers.slice(0, 5);
    // newCarData will temporarely hold old and valid new car data
    const newCarData = { ...state.carData };

    for (const newVinNumber of newVinNumbers) {
      // Only fetch new VIN numbers data because the API doesn't suppoprt multiple numbers and the operation is expensive
      if (!state.vinNumbers.includes(newVinNumber)) {
        try {
          const res = await http.$get(`/api/vehicles/decodevin/${newVinNumber}?format=json`);
          // const res = await mockCarData;
          if (res && res.Results) {
            newCarData[newVinNumber] = res.Results;
          }
        } catch (e) {
          // ToDo: proper error handling
        }
      }
    }

    // Filter out old data from newCarData
    commit('setCarData', newVinNumbers.reduce((acc, vin) => ({ ...acc, [vin]: newCarData[vin] }), {}));
    commit('setVinNumbers', newVinNumbers);
    commit('setIsLoading', false);
  }
};

export default actions;
