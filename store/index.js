import { CAR_DATA_VALID_DATA_ROWS } from '@/constants/carData';

export const state = () => ({
  isLoading: false,
  /** Holds the ordered list of VIN numbers for the cars, with default list */
  vinNumbers: [],
  /** Car data for the vinNumbers list */
  carData: {}
});

export const mutations = {
  setIsLoading: (state, isLoading = true) => {
    state.isLoading = isLoading;
  },
  setVinNumbers: (state, vinNumbers) => {
    state.vinNumbers = vinNumbers;
  },
  setCarData: (state, carData) => {
    state.carData = carData;
  }
};

export const getters = {
  /** Get individual car data by VIN number, returns only fields declared in CAR_DATA_VALID_DATA_ROWS */
  carData: state => vin => state.carData[vin] ? state.carData[vin].map(
    dataRow => CAR_DATA_VALID_DATA_ROWS.includes(dataRow.Variable) && dataRow.Value !== null && { label: dataRow.Variable, value: dataRow.Value }).filter(
    dataRow => !!dataRow) : {}
};
