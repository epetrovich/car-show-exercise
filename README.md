# car-show-exercise

Code challenge based on the following acceptance criteria:

- Build a project using Vue.js and Nuxt.
- It should be centered around one of your passions - Such as cars.
- You should build an application that integrates with an external API to pull in data for five different cars.
- It should utilize state management with Store / VueX, Vue router, etc 

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

## Notes
To complete the challenge, I used a freely available API from [NHTSA](https://vpic.nhtsa.dot.gov/).  While this API provides a lot of information about vehicles, the main purpose of it is for VIN (Vehicle Identification Number) decoding/checking, therefore it has no endpoint to get list of cars.
I considered several other open APIs, but found no other that would suit the purpose.

To satisfy the requirements of the challenge with the available resources, I based the car list in an (editable) input that would hold 5, comma separated, different VINs.  The field has already 5 pre populated codees.  
Because of the API does not support multiple vehicle requests, a single request has to be done for each VIN number.  I consider this when calculating what data to fetch when there are changes, to try to minimize the number of calls.

As requested, I used Vuex, and Vue Router.  For this last one I added an "About" page just to demonstrate that is working properly.

### Nice to have but not included because of time constraints and scope of the challenge:
- Nicer VIN numbers input and validation.
- Better accessibility.
- More Component tests.
- API error handling (unhappy path).
- Custom error pages.
- Persistence of the store

