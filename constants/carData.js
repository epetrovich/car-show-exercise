export const CAR_DATA_TITLE_VALUES = [
  'Model Year',
  'Make',
  'Model'
];

export const CAR_DATA_VALID_DATA_ROWS = [
  'Make',
  'Model',
  'Model Year',
  'Series',
  'Trim',
  'Body Class',
  'Doors',
  'Transmission Speeds',
  'Engine Number of Cylinders'
];

export const CAR_DATA_DEFAULT_VIN_NUMBERS = [
  '1J4GZ78Y7TC214178',
  '5YJ3E1EA5KF328931',
  'WVWCA0157HK036627',
  '1FMEU15HXFLA29734',
  '1JTBG64S0H0014395'
];
